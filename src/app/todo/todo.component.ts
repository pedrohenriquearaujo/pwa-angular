import { DisciplinaService } from '../services/disciplinaService.service';
import { disciplina } from './../model/disciplina';
import { professor } from './../model/professor';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfessorService } from '../services/professorService.service';


@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css'], 
  
  providers: [ProfessorService]
})
export class TodoComponent implements OnInit {
  displayedColumnsDisciplina: string[] = ['matricula','nome','email'];
  ProfessorService: any;
  professores: professor[] = [];
  private dataSource:any[] = [];
  disciplina: any;
  professor: any;
  nome: any;
  n: string
 

  constructor(private activatedRoute: ActivatedRoute, private professorService : ProfessorService, private disciplinaService : DisciplinaService, private router: Router) {
            
   }

  ngOnInit() {
    this.carregarListaTodosProfessores()
  }

  getRecord(row){
    this.professor = row;
    this.router.navigateByUrl('detalhe/'+ this.professor.nome.split(" ")[0].toLowerCase(),this.professor);
  }

  carregarListaTodosProfessores() {
    
    this.professorService.getProfessores().subscribe(
      data => {
        this.dataSource = data             
      },
      error => {
        console.log(error);
      });

      ;
  }


}
