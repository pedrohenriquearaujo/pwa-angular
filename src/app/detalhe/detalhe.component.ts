import { Component, OnInit } from '@angular/core';
import { DisciplinaService } from '../services/disciplinaService.service';
import { disciplina } from '../model/disciplina';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfessorService } from '../services/professorService.service';
import { professor } from '../model/professor';
import { stringify } from '@angular/compiler/src/util';

@Component({
  selector: 'app-detalhe',
  templateUrl: './detalhe.component.html',
  styleUrls: ['./detalhe.component.css'],
  providers: [ProfessorService]

})
export class DetalheComponent implements OnInit {
  displayedColumnsDisciplina: string[] = ['codigo','descricao','qtdCreditos'];
  displayedColumnsProfessor: string[] = ['nome','matricula','email'];
  ProfessorService: any;
  dataSource: disciplina[];
  dataSourceProfessor: professor[] = []; 
  matriculaProfessor : string;
  p: professor ;

  constructor(private activatedRoute: ActivatedRoute, private professorService : ProfessorService, private disciplinaService : DisciplinaService, private router: Router) {
  
  }

 ngOnInit() {
  this.p = new professor();
  this.p.nome = "";
  this.p.matricula = "";
  this.p.email = "";
  this.matriculaProfessor = this.activatedRoute.snapshot.paramMap.get("nome")
  this.carregarListaTodasDisciplinas(this.matriculaProfessor)
  this.carregarListaTodosProfessores();
   
 }

 carregarListaTodasDisciplinas(matriculaProfessor: string) {
    
  this.disciplinaService.getDisciplinasListar(matriculaProfessor).subscribe(
    data => {
      this.dataSource = data
    },
    error => {
      console.log(error);
    });
}

carregarListaTodosProfessores() {
    
  this.professorService.getProfessores().subscribe(
    data => {
      data.forEach(element => {
        if(element.nome.split(" ")[0].toLowerCase() == this.matriculaProfessor){
          this.p = element;
        }
      });           
    },
    error => {
      console.log(error);
    });    
}
}
