import { professor } from './../model/professor';
import {  disciplina } from '../model/disciplina';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfessorService {

  constructor(private http:HttpClient) { }

private baseUrl:string = 'http://demo3803018.mockable.io'
private metodo:string = '/listar'
private professoresUrl:string = '/professores'

getProfessores() : Observable<professor[]>{
  return this.http.get<professor[]>(this.baseUrl + this.metodo + this.professoresUrl)
}

getDisciplinasByProfessor(matricula:string) : Observable<disciplina[]>{
  return this.http.get<disciplina[]>(this.baseUrl+"professor/")
}


}
