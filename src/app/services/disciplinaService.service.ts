import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { disciplina } from '../model/disciplina';

@Injectable({
  providedIn: 'root'
})
export class DisciplinaService {

  constructor(private http:HttpClient) { }

private baseUrl:string = 'http://demo3803018.mockable.io/'
private metodo:string = 'listar/'
private disciplinasUrl:string = '/disciplinas'

getDisciplinasListar(url:string) : Observable<disciplina[]>{
  return this.http.get<disciplina[]>(this.baseUrl+url+this.disciplinasUrl)
}

}
