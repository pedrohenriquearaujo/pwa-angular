import { BrowserModule,  } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatToolbarModule,
  MatCardModule,
  MatGridListModule

} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule ,Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table'; 

import { AppComponent } from './app.component';
import { TodoComponent } from './todo/todo.component';
import { DetalheComponent } from './detalhe/detalhe.component';

const appRoutes: Routes = [
  {
    path:'detalhe/:nome',
    component:DetalheComponent
  },
  {
    path:'',
    component:TodoComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    DetalheComponent
  ],
  imports: [
    MatGridListModule,
    MatCardModule,
    MatTableModule,
    BrowserModule,
    RouterModule.forRoot(
    appRoutes,
      { enableTracing: true }
    ),
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatListModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    FlexLayoutModule,
    RouterModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
